var app = angular.module('adminApp', []);
app.controller('DataControler', function($scope, $http) {
    var userUrl = 'https://sitailung.herokuapp.com/users/';
    var userid = 0;
    if (localStorage.getItem("userid") != null) {
        userid = localStorage.getItem("userid");
        $http.get(userUrl)
            .then(function(response) { 
                $scope.users = response.data;
            });
        
        // Handle Delete user account
        $scope.remove = function(index) {
            $http.delete(userUrl + index)
            .then(function(response) { 
                alert(response.data.message);
                // Refresh the data after deleting an account
                window.location.href = "../views/admin.html";
            });
        };
    } else {
        window.location.href = "../views/LoginAndRegister.html";
    }
});