var app = angular.module('bookmarkApp', []);
app.controller('DataControler', function($scope, $http) {
    var bookmarkUrl = 'https://sitailung.herokuapp.com/bookmarks/';
    var weatherUrl = 'https://sitailung.herokuapp.com/weathers/';
    var userid = 0;
    // If the user is logined, get its all bookmark
    if (localStorage.getItem("userid") != null) {
        userid = localStorage.getItem("userid");
        var url = bookmarkUrl+userid;
        $http.get(url)
            .then(function(response) { 
                if (response.data == null) {
                    $scope.bookmarks = [["Empty!"]];
                    $('#clearBookmark').hide();
                }
                else {
                    $scope.bookmarks = response.data.cities;
                    console.log(response.data.cities);
                }
            });
        // Handle getMoreInfo
        $scope.getMore = function(cityName) {
            if(cityName != "Empty!"){
                $http.get(weatherUrl + cityName + '?units=Metric')
                    .then(function(response) { 
                        alert("Temperture: " + response.data.temp 
                        + "\nTemperture Range is: " + response.data.min 
                        + " ~ " + response.data.max 
                        + "\nDescription: " + response.data.desc);
                    });
            } else {
                alert("That is funny, have a good day!");
            }
        };
        
        // Handle clearBookmark
        $('#clearBookmark').click(function() {
            $http.delete(url)
            .then(function(response) { 
                alert(response.data.message);
                // Refresh the bookmark data
                window.location.href = "../views/bookmark.html";
            });
        });
    } else {
        window.location.href = "../views/LoginAndRegister.html";
    }
});