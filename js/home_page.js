if (localStorage.getItem("username") == null) {
    // Show Login/Register if no user login
    $('#login_register').show();
    // Hide logout and greeting
    $('#logout').hide();
    $('#greeting').hide();
}
else {
    // Show greeting message & Log Out if user logined
    $('#logout').show();
    $('#greeting').show();
    // Hide Login/Register
    $('#login_register').hide();
}

// When user click log out button, clear the login info from local storage
function clearStore(){
    localStorage.clear();
    $('#login_register').show();
    $('#logout').hide();
    $('#greeting').hide();
}