$(function() {

    var users = [];
    var dif_username = false;
    var check = true;
    var curUserID = 0;
    
    // Controll the login and register form show and hide
    $('#login-form-link').click(function(e) {
        $("#login-form").delay(100).fadeIn(100);
        $("#register-form").fadeOut(100);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });
    $('#register-form-link').click(function(e) {
        $("#register-form").delay(100).fadeIn(100);
        $("#login-form").fadeOut(100);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });


    var userUrl = 'https://sitailung.herokuapp.com/users/';
    var userid = 0;
    $.get(userUrl, function(data) {
        // alert(data);
        console.log(data);
        console.log(data.length);
        users = data;
        userid = data.length;
    });


    // Handle Login
    $('#login-submit').click(function(e) {
        var uname = $('#uname').val();
        var pwd = $('#pwd').val();
        if(chkuserexist(uname, pwd)){
            alert("Welcome back " + uname);
            // Store user's logined info to lacal storage
            localStorage.setItem("username", uname);
            localStorage.setItem("password", pwd);
            localStorage.setItem("userid", curUserID);
            // redirect to the home page or admin page base on the username 
            if (uname == "admin"){
                window.location.href = "../views/admin.html";
            } else {
                window.location.href = "../views/home_page.html";
            }
        } else {
            alert("The username does not exist or the password is not correct!");
        }
    });
    
    function chkuserexist(uname, pwd){
        var exist = false;
        for (var i = 0; i < users.length; i++) {
            if (users[i].username == uname) {
                if (users[i].password == pwd){
                    curUserID = users[i].userid;
                    exist =  true;
                } else {
                    exist = false;
                }
                break;
            } else {
                exist = false;
            }
        }
        return exist;
    }

    // Validate the username
    var chkusername = $('#username').change(function() {
        var username = $('#username').val();
        if (users.length == 0) {
            dif_username = true;
        }
        else {
            for (var i = 0; i < users.length; i++) {
                console.log(users[i].username);
                if (users[i].username == username) {
                    alert("This username has already used! Please input another one!");
                    dif_username = false;
                }
                else {
                    dif_username = true;
                }
            }
        }
        return dif_username;
    });

    // Validate the email
    var chkemail = $('#email').change(function() {
        var email = $('#email').val();
        if (!email || (email.length < 6) || (email.indexOf('@') == -1)) {
            alert('Please enter a valid email address!');
            check = false;
        } else {
            check = true;
        }
    });

    // Validate the password
    var chkpassword = $('#password').change(function() {
        var password = $('#password').val();
        if (!password || (password.length < 1)) {
            alert('Please enter a password!');
            check = false;
        } else {
            check = true;
        }
    });
    
    // Validate the confirm_password
    var chkconfirm_password = $('#confirm_password').change(function() {
        var password = $('#password').val();
        var confirm_password = $('#confirm_password').val();
        if (password != confirm_password) {
            alert('Confirm password and password are different!');
            check = false;
        } else {
            check = true;
        }
    });

    // Handle Register
    $('#register-submit').click(function() {
        // Ensure the input are valid
        if (chkusername && check) {
            var username = $('#username').val();
            var email = $('#email').val();
            var password = $('#password').val();
            var data = {
                userid: userid,
                username: username,
                email: email,
                password: password
            };
            $.post(userUrl, data, function(data, status) {
                alert("Register Status: " + status + "\nYour username: " + username + "; password: " + password);
                userid++;
                // Reset input 
                $('#userid').val(userid);
                $('#username').val("");
                $('#email').val("");
                $('#password').val("");
                $('#confirm_password').val("");
                window.location.href = "../views/LoginAndRegister.html";
            });
            
        }
        else {
            alert("Register fail!");
        }

    });
});
