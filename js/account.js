var app = angular.module('accountApp', []);
app.controller('DataControler', function($scope, $http) {
    var userUrl = 'https://sitailung.herokuapp.com/users/';
    var userid = 0;
    // If the is logined, get this user info with its id from local storage
    if (localStorage.getItem("userid") != null) {
        userid = localStorage.getItem("userid");
        var url = userUrl+userid;
        $http.get(url)
            .then(function(response) { 
                $scope.user = response.data;
                console.log(response.data);
            });
    } else {
        // If not logined, direct to Login page
        window.location.href = "../views/LoginAndRegister.html";
    }
});