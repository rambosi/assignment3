// Declare a module object called 'app'
var app = angular.module('myApp', []);
// Set the controller for app
app.controller('DataControler', function($scope, $http) {
    var weatherUrl = 'https://sitailung.herokuapp.com/weathers/'
    var picUrl = 'http://openweathermap.org/img/w/';
    
    //Get the most up-to-date weather info from
    $('#search').click(function() {
        var city = $('#city').val();
        var index = document.getElementById('units').selectedIndex;
        var units = "";
        if (index == 0){
            units = "Kelvin";
        }else if (index == 1){
            units = "Metric";
        }else{
            units = "Imperial";
        }
        var url = weatherUrl + city + '?units=' + units;
        console.log(url);
        $http.get(url)
        .then(function(response){
            $scope.cityName = response.data.cityName;
            $scope.temp = response.data.temp;
            $scope.min = response.data.min;
            $scope.max = response.data.max;
            $scope.desc = response.data.desc;
            $scope.iconUrl = picUrl + response.data.iconUrl + ".png";
            console.log(response.data);
        });
    });

    $('#bookmark').click(function() {
        // Get logined user id
        var uid = localStorage.getItem("userid");
        var city = $('#city').val();
        if (uid == null){
            alert("Please login first!");
            window.location.href = "../views/LoginAndRegister.html";
        } else {
            var url = 'https://sitailung.herokuapp.com/bookmarks/';
            // Check whether bookmark is empty
            $http.get(url + uid)
            .then(function(response){
                if (response.data == null) {
                    // Create a bookmark with post method
                    var newBookmark = {
                        "cities": [
                            [city]
                        ],
                        "userid":uid
                    };
                    $http.post(url,newBookmark)
                    .then(function successCallback(response) {
                        alert("Bookmark added successfully!");
                    }, function errorCallback(response) {
                        alert("Bookmark added fail!");
                    });
                }
                else {
                    // Add a bookmark with put method
                    var bookmark = [city];
                    $http.put(url + uid, bookmark)
                    .then(function successCallback(response) {
                        alert("Bookmark added successfully!");
                    }, function errorCallback(response) {
                        alert("Bookmark added fail!");
                    });
                }
                console.log(response.data);
            });
        }
    });
});
